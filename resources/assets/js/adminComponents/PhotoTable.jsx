import React from 'react';
import $ from 'jquery';
import Actions from '../actions/adminActions';
import connectToStores from 'alt-utils/lib/connectToStores';
import AdminStore from '../stores/AdminStore';
import PhotoData from './PhotoData.jsx';
import Griddle from 'griddle-react';

@connectToStores
class PhotoTable extends React.Component {
  constructor() {
    super();
    this.state = {
      validTables : ["accepts", "rejects"],
      photoType : undefined,
      data : [],
      recentTotal : 0,
      lastPhotoId : undefined,
      currentPageNum : 0,
      maxPages: 0,
      externalResultsPerPage: 20,
      externalSetPageSize: 20,
      columnMetaData: [
        {
          "columnName" : "id",
          "visible" : false
        },
        {
          "columnName" : "city",
          "displayName" : "City"
        },
        {
          "columnName" : "state_region",
          "displayName" : "State / Region"
        },
        {
          "columnName" : "country",
          "displayName" : "Country"
        },
        {
          "columnName" : "photo_data",
          "displayName" : "Photo"
        },
        {
          "columnName" : "buttons",
          "displayName" : "",
          "customComponent" : PhotoData
        },
      ]
    }
    Actions.getUserData();
  }

  componentDidMount() {
    console.log(this.props);

    var queryData = this.props.location.query,
        tableType;

    if (!queryData.hasOwnProperty("photoType")) { window.location.hash = "/dashboard"; }
    tableType = queryData.photoType;

    if (this.state.validTables.indexOf(tableType) < 0) {
        window.location.hash = "/dashboard";
    }
    this.setState({
      photoType : tableType
    }, () => {
      if (this.props[tableType].length == 0) {
        this.getPhotoData(0, undefined, true);
      }
    });

  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps[this.state.photoType].length);

    if (nextProps[this.state.photoType].length > this.state.data.length) {
      this.setState({
        data : this.state.data.concat(nextProps[this.state.photoType]),
        maxPages : Math.ceil(nextProps.totalPhotos / this.state.externalResultsPerPage)
      });
    }
  }

  getPhotoData(page, lastPhotoId, isfreshFilter) {
    var self = this,
        page = page || 1;

    Actions.listMorePhotosForAdmin({
      tableType : self.state.photoType,
      lastId : self.props.lastPhotoId,
      locations : {
        country : "",
        stateRegion : "",
        city : ""
      },
      freshFilter : isfreshFilter
    });

    this.setState({
      currentPageNum: page-1,
      maxPages: Math.ceil(self.props.totalPhotos / self.state.externalResultsPerPage)
    }, () => {
      console.log('current page num');
      console.log(self.state.currentPageNum);
    });
  }

  setPage(index) {
    if (this.state.currentPageNum > index) {
        Actions.removePhotosFromAdmin({
          photoType: this.state.photoType,
          photoIndex: this.props[this.state.photoType].length - this.state.externalResultsPerPage
        });
        this.setState({
          currentPageNum: index,
          maxPages: Math.ceil(self.props.totalPhotos / self.state.externalResultsPerPage)
        });
    } else {
        var index = index > this.state.maxPages ? this.state.maxPages : index < 1 ? 1 : index + 1;
        this.getPhotoData(index, this.props.lastPhotoId, false);
    }
  }

  static getStores() {
    return [AdminStore];
  }

  static getPropsFromStores() {
    return AdminStore.getState();
  }

  render() {
    console.log(this.state.data);
    var photoTable = <div></div>;
    if (this.state.data.length > 0) {
      photoTable = <Griddle
                    useExternal={true}
                    results={this.state.data}
                    columns={["city", "state_region", "country", "photo_data", "buttons"]}
                    columnMetadata={this.state.columnMetaData}
                    showFilter={true}
                    externalSetPage={this.setPage.bind(this)}
                    externalCurrentPage={this.state.currentPageNum}
                    externalMaxPage={this.state.maxPages}
                    externalSetPageSize={function() {}}
                    externalSortColumn={null}
                    externalSortAscending={true}
                    externalChangeSort={function() {}}
                    externalSetFilter={function() {}}
                    resultsPerPage={this.state.externalResultsPerPage}
                  />;
    } else {
      photoTable = <div>Loading ....</div>;
    }

    return (
      <div>
        {photoTable}
      </div>
    );
  }
}

export default PhotoTable;
