<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/style.css') }}" type="text/css" />

	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="{{ asset('css/sladStyles/onepage.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ asset('css/sladStyles/main.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/font-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/et-line.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/magnific-popup.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/sladStyles/vmap.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ asset('css/sladStyles/responsive.css') }}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="{{ asset('js/sladJs/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/sladJs/jquery.vmap.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/sladJs/jquery.vmap.sampledata.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/sladJs/jquery.vmap.usa.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/sladJs/plugins.js') }}"></script>

	<!-- Document Title
	============================================= -->
	<title>SLAD</title>

</head>

<body class="stretched light side-push-panel no-transition">

	<div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">

			<div class="widget widget_links clearfix">

				<h4>About SLAD</h4>

				<div style="font-size: 14px; line-height: 1.7;">
					<!--<address style="line-height: 1.7;">
						795 Folsom Ave, Suite 600<br>
						San Francisco, CA 94107<br>
					</address>-->

					<div class="clear topmargin-sm"></div>

					<!--<abbr title="Phone Number">Phone:</abbr> (91) 8547 632521<br>
					<abbr title="Fax">Fax:</abbr> (91) 11 4752 1433<br>-->
					<abbr title="Email Address">Email:</abbr>
					<a href="mailto:info@safetylightalertingdrivers.com">info@safetylightalertingdrivers.com</a>
				</div>

			</div>

			<!--<div class="widget quick-contact-widget clearfix">

				<h4>Connect Socially</h4>

				<a href="#" class="social-icon si-small si-colored si-facebook" title="Facebook">
					<i class="icon-facebook"></i>
					<i class="icon-facebook"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-twitter" title="Twitter">
					<i class="icon-twitter"></i>
					<i class="icon-twitter"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-github" title="Github">
					<i class="icon-github"></i>
					<i class="icon-github"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-pinterest" title="Pinterest">
					<i class="icon-pinterest"></i>
					<i class="icon-pinterest"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-forrst" title="Forrst">
					<i class="icon-forrst"></i>
					<i class="icon-forrst"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-linkedin" title="LinkedIn">
					<i class="icon-linkedin"></i>
					<i class="icon-linkedin"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-gplus" title="Google Plus">
					<i class="icon-gplus"></i>
					<i class="icon-gplus"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-instagram" title="Instagram">
					<i class="icon-instagram"></i>
					<i class="icon-instagram"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-flickr" title="Flickr">
					<i class="icon-flickr"></i>
					<i class="icon-flickr"></i>
				</a>
				<a href="#" class="social-icon si-small si-colored si-skype" title="Skype">
					<i class="icon-skype"></i>
					<i class="icon-skype"></i>
				</a>

			</div>-->

		</div>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header dark transparent-header static-sticky" data-sticky-offset="full" data-sticky-offset-negative="100">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.html" class="standard-logo">SLAD</a>
						<a href="index.html" class="retina-logo">SLAD</a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
							<li><a href="#" data-href="#wrapper"><div>Home</div></a></li>
							<li><a href="#" data-href="#section-about"><div>About</div></a></li>
							<li><a href="#" data-href="#section-examples"><div>Why Us?</div></a></li>
							<li><a href="#" data-href="#section-services"><div>Known Facts</div></a></li>
							<li><a href="mailto:info@safetylightalertingdrivers.com" target="_top"><div>Contact</div></a></li>
						</ul>

						<!--<div id="side-panel-trigger" class="side-panel-trigger"><a href="#"><i class="icon-reorder"></i></a></div>-->

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-parallax full-screen force-full-screen">

			<div class="full-screen force-full-screen dark section nopadding nomargin noborder ohidden">

				<div class="container center">
					<div class="vertical-middle ignore-header">
						<div class="emphasis-title">
							<h1>
								<span class="text-rotater nocolor" data-separator="|" data-rotate="fadeIn" data-speed="2000">
									<span class="t-rotate t700 font-body opm-large-word">Safety|Light|Alerting|Drivers</span>
								</span>
							</h1>
							<h2 style="font-size: 46px;">
								Coming to an Intersection Near You.
							</h2>
						</div>
						<a href="#" class="button button-border button-light button-rounded" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Who are we?</a>
					</div>
				</div>

				<div class="video-wrap">
                    <video id="slide-video" poster="{{ asset('sladVideos/videos/1.jpg') }}" preload="auto" loop muted autoplay>
                        <source src="{{ asset('sladVideos/TLmaidan_2.wmv') }}" type='video/wmv' />
                        <source src="{{ asset('sladVideos/TLmaidan_2.mp4') }}" type='video/mp4' />
                    </video>
                    <div class="video-overlay" style="background-color: rgba(0, 124, 255, 0.8);"></div>
                </div>

				<a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="65" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

			</div>

		</section><!-- #slider end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nopadding">

				<div id="section-about" class="center page-section">

					<div class="container clearfix">

						<h2 class="divcenter bottommargin font-body" style="max-width: 700px; font-size: 34px;">S.L.A.D. an acronym for Safety Light Alerting Drivers is an innovative technology company to decrease fatalities &amp; casualties among the emergency responders, motorist, and pedestrians.</h2>

						<p class="lead divcenter bottommargin" style="max-width: 800px;">
							We are here to inform, warn, and protect our loved ones by deploying S.L.A.D. strobe beacon lights on every intersection nationwide and internationally. S.L.A.D. device will alert drivers, pedestrians, cyclists, deaf and hearing impaired during an emergency first responders call. This innovative and much needed device will allow a clear path for first responders to pursue an emergency without being impeded and gives motorist advanced notice to merge safely out of harm's way, which will ultimately improve emergency response times.
						</p>

						<!--<p class="bottommargin" style="font-size: 16px;"><a href="#" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70" class="more-link">Learn more <i class="icon-angle-right"></i></a></p>-->

						<div class="clear"></div>

						<!--<div class="row topmargin-lg divcenter" style="max-width: 1000px;">

							<div class="col-sm-4 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="{{ asset('images/team/1.jpg') }}" alt="John Doe">
										<div class="team-overlay">
											<div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-facebook" title="Facebook">
													<i class="icon-facebook"></i>
													<i class="icon-facebook"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-github" title="Github">
													<i class="icon-github"></i>
													<i class="icon-github"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
									</div>
								</div>

							</div>

							<div class="col-sm-4 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="{{ asset('images/team/2.jpg') }}" alt="Josh Clark">
										<div class="team-overlay">
											<div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-linkedin" title="LinkedIn">
													<i class="icon-linkedin"></i>
													<i class="icon-linkedin"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-flickr" title="Flickr">
													<i class="icon-flickr"></i>
													<i class="icon-flickr"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Mary Jane</h4><span>Co-Founder</span></div>
									</div>
								</div>

							</div>

							<div class="col-sm-4 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="{{ asset('images/team/3.jpg') }}" alt="Mary Jane">
										<div class="team-overlay">
											<div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-vimeo" title="Vimeo">
													<i class="icon-vimeo"></i>
													<i class="icon-vimeo"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-instagram" title="Instagram">
													<i class="icon-instagram"></i>
													<i class="icon-instagram"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Josh Clark</h4><span>Sales</span></div>
									</div>
								</div>

							</div>

						</div>-->

					</div>

				</div>

				<div id="section-examples" class="page-section notoppadding">

					<div class="section nomargin">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 900px;">
								<h2 class="divcenter bottommargin font-body" style="max-width: 700px; font-size: 34px;">Here are some scenarios S.L.A.D will prevent.</h2>
							</div>
						</div>
					</div>

					<div class="container clearfix">
						<div class="masonry-thumbs col-2 clearfix">
							<a href="https://www.youtube.com/watch?v=V_xhEXiu9HM" data-lightbox="iframe">
								<img src="{{ asset('images/accident-example2.png') }}" alt="Vimeo Video">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-youtube-play"></i></div></div>
							</a>
							<a href="https://www.youtube.com/watch?v=Log9l-P88k8" data-lightbox="iframe">
								<img src="{{ asset('images/accident-example1.png') }}" alt="Youtube Video">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-youtube-play"></i></div></div>
							</a>
						</div>

						<div class="masonry-thumbs col-4 clearfix" data-big="3" data-lightbox="gallery">
							<a href="{{ asset('images/accPic3.jpg') }}" data-lightbox="gallery-item">
								<img src="{{ asset('images/accPic3.jpg') }}" alt="Gallery Image">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-line-plus"></i></div></div>
							</a>
							<a href="{{ asset('images/accPic.jpg') }}" data-lightbox="gallery-item">
								<img src="{{ asset('images/accPic.jpg') }}" alt="Gallery Image">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-line-plus"></i></div></div>
							</a>
							<a href="{{ asset('images/accPic2.jpg') }}" data-lightbox="gallery-item">
								<img src="{{ asset('images/accPic2.jpg') }}" alt="Gallery Image">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-line-plus"></i></div></div>
							</a>
							<a href="{{ asset('images/accPic4.jpg') }}" data-lightbox="gallery-item">
								<img src="{{ asset('images/accPic4.jpg') }}" alt="Gallery Image">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-line-plus"></i></div></div>
							</a>
							<a href="{{ asset('images/accPic5.jpg') }}" data-lightbox="gallery-item">
								<img src="{{ asset('images/accPic5.jpg') }}" alt="Gallery Image">
								<div class="overlay"><div class="overlay-wrap"><i class="icon-line-plus"></i></div></div>
							</a>
						</div>
					</div>
				</div>

				<!--<div id="section-works" class="page-section notoppadding">

					<div class="section nomargin">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 900px;">
								<h2 class="nobottommargin t300 ls1">We create &amp; craft projects that ooze creativity in every aspect. We try to create a benchmark in everything we do. Take a moment to browse through some of our recent completed work.</h2>
							</div>
						</div>
					</div>-->

					<!-- Portfolio Items
					============================================= -->
					<!--<div id="portfolio" class="portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry clearfix">

						<article class="portfolio-item pf-media pf-icons wide">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/1.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Open Imagination</a></h3>
										<span><a href="#">Media</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-illustrations">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/2.jpg" alt="Locked Steel Gate">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Locked Steel Gate</a></h3>
										<span><a href="#">Illustrations</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-graphics pf-uielements">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/3.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Mac Sunglasses</a></h3>
										<span><a href="#">Graphics</a>, <a href="#">UI Elements</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-media pf-icons wide">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/4.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Open Imagination</a></h3>
										<span><a href="#">Media</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-media wide">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/5.jpg" alt="Console Activity">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Console Activity</a></h3>
										<span><a href="#">UI Elements</a>, <a href="#">Media</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-media pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/6.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Open Imagination</a></h3>
										<span><a href="#">Media</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/mixed/7.jpg" alt="Backpack Contents">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Backpack Contents</a></h3>
										<span><a href="#">UI Elements</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

					</div>--><!-- #portfolio end -->

					<!-- Portfolio Script
					============================================= -->
					<!--<script type="text/javascript">

						jQuery(window).load(function(){

							var jQuerycontainer = jQuery('#portfolio');

							jQuerycontainer.isotope({
								transitionDuration: '0.65s',
								masonry: {
									columnWidth: jQuerycontainer.find('.portfolio-item:not(.wide)')[0]
								}
							});

							jQuery(window).resize(function() {
								jQuerycontainer.isotope('layout');
								SEMICOLON.portfolio.portfolioDescMargin();
							});

							var t = setTimeout(function(){ SEMICOLON.portfolio.portfolioDescMargin(); }, 200);

						});

					</script>--><!-- Portfolio Script End -->

					<!--<div class="topmargin center"><a href="#" class="button button-border button-circle t600">View More Projects</a></div>

				</div>-->

				<div id="section-services" class="page-section notoppadding nobottompadding">

					<!--<div class="section nomargin">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 900px;">
								<h2 class="nobottommargin t300 ls1">We enjoy working on the Services &amp; Products we provide as much as you need them. This help us in delivering your Goals easily. Browse through the wide range of services we provide.</h2>
							</div>
						</div>
					</div>-->

					<!--<div class="common-height clearfix">

						<div class="col-md-4 hidden-xs" style="background: url('../images/services/main-bg.jpg') center center no-repeat; background-size: cover;"></div>
						<div class="col-md-8">
							<div class="max-height">
								<div class="row common-height grid-border clearfix">
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-mobile"></i></a>
											</div>
											<h3>Responsive Framework</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-presentation"></i></a>
											</div>
											<h3>Beautifully Presented</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-puzzle"></i></a>
											</div>
											<h3>Extensively Extendable</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-gears"></i></a>
											</div>
											<h3>Powerfully Customizable</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-genius"></i></a>
											</div>
											<h3>Geniusly Transformable</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<a href="#"><i class="icon-et-hotairballoon"></i></a>
											</div>
											<h3>Industrial Support</h3>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>-->

					<!--<div class="section dark nomargin">
						<div class="divcenter center" style="max-width: 900px;">
							<h2 class="nobottommargin t300 ls1">Like Our Services? Get an <a href="#" data-scrollto="#template-contactform" data-offset="140" data-easing="easeInOutExpo" data-speed="1250" class="button button-border button-circle button-light button-large notopmargin nobottommargin" style="position: relative; top: -3px;">Instant Quote</a></h2>
						</div>
					</div>-->

					<div id="section-stats" class="section page-section nobottommargin">
					<div class="container clearfix">

						<div class="col_three_fifth nobottommargin known-facts center">
								<h1 style="font-size: 26px;">National Average Emergency Vehicle Response Time</h1>
								<hr/>
								<div style="font-size: 30px;" class="counter counter-medium">
									<span data-from="0" data-to="15" data-refresh-interval="100" data-speed="3000"></span>
										Minutes and
									<span data-from="0" data-to="19.2" data-refresh-interval="100" data-speed="3000"></span>
										Seconds
								</div>
								<div id="vmap2" style="height: 400px;"></div>
						</div>

						<div class="row" style="padding-top: 20px;">
							<div class="col-md-6">


								<h4>Top 5 Longest Response Times</h4>

								<ul class="list-group">
									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="35" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="44.4" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Wyoming
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="22" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="56.4" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Vermont
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="22" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="34.8" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Montana
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="21" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="30" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										North Dakota
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="21" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="22.2" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Kansas
									</li>
								</ul>
							</div>
							<div class="col-md-6">
								<h4>Top 5 Shortest Response Times</h4>

								<ul class="list-group">
									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="6" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="0" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										<span class="state">Illinois</span>
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="6" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="51.6" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										California
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="7" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="3.6" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Rhode Island
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="7" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="54" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Connecticut
									</li>

									<li class="list-group-item">
										<div class="counter badge counter-small">
											<span data-from="0" data-to="8" data-refresh-interval="100" data-speed="3000"></span>
											min,
											<span data-from="0" data-to="33" data-refresh-interval="100" data-speed="3000"></span>
											sec
										</div>
										Massachusetts
									</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<p class="center">Average Response Time (Minutes)</p>
							<div class="progress aqua" data-width="0%">
								<div class="progress-bar">
									<div class="progress-text left">6.00</div>
									<div class="progress-text right">35.74</div>
								</div>
							</div>
							<p class="not-select center">
								<span></span>
									Not Sufficient Data
							</p>
						</div>
					</div>
				</div>

					<div class="section parallax nomargin dark" style="background-image: url('{{ asset('images/ambulanceCrash.jpg') }}'); background-size: cover; padding: 150px 0;" data-stellar-background-ratio="0.3">

						<div class="container clearfix">

							<div class="col_two_fifth nobottommargin">&nbsp;</div>

							<div class="col_three_fifth nobottommargin known-facts">

								<div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide">
												<div class="testi-content">
													<p>
														There were an estimated 4,500 accidents involving ambulances each year from 1992
through 2011; 35% of which resulted in injury or fatality to at least one occupant of a
vehicle involved.
													</p>
													<!--<div class="testi-meta">
														Steve Jobs
														<span>Apple Inc.</span>
													</div>-->
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>
														Of those killed in an ambulance accident, 63% were occupants of a passenger vehicle, 21% were passengers in the ambulance, 4% were the ambulance driver, and 12% were non-occupants.
													</p>
													<!--<div class="testi-meta">
														Collis Ta'eed
														<span>Envato Inc.</span>
													</div>-->
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>
														Nearly 60% of ambulance accidents occur during the course of emergency
use.
													</p>
													<!--<div class="testi-meta">
														John Doe
														<span>XYZ Inc.</span>
													</div>-->
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>
														About 70% of all fire truck accidents occurred while in emergency use.
													</p>
													<!--<div class="testi-meta">
														John Doe
														<span>XYZ Inc.</span>
													</div>-->
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>
														Vehicle fatality rates for emergency responders are up to 4.8 times higher than other
accidents.
													</p>
													<!--<div class="testi-meta">
														John Doe
														<span>XYZ Inc.</span>
													</div>-->
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>

				<!--<div id="section-blog" class="page-section">

					<h2 class="center uppercase t300 ls3 font-body">Recently From the Blog</h2>

					<div class="section nobottommargin">
						<div class="container clearfix">

							<div class="row topmargin clearfix">

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/1.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Press &amp; Media</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Global Meetup Program is Launching!</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/2.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Inside Scoops</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">The New YouTube Economy unfolds itself</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/3.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Video Blog</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Kicking Off Design Party in Style</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/4.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Inspiration</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Top Ten Signs You're a Designer</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

					<div class="container topmargin-lg clearfix">

						<div id="oc-clients" class="owl-carousel topmargin image-carousel">

							<div class="oc-item"><a href="#"><img src="../images/clients/1.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/2.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/3.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/4.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/5.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/6.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/7.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/8.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/9.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/10.png" alt="Clients"></a></div>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function($) {

								var ocClients = $("#oc-clients");

								ocClients.owlCarousel({
									margin: 80,
									loop: true,
									nav: false,
									autoplay: true,
									dots: false,
									autoplayHoverPause: true,
									responsive:{
										0:{ items:2 },
										480:{ items:3 },
										768:{ items:4 },
										992:{ items:5 },
										1200:{ items:6 }
									}
								});

							});

						</script>

					</div>

				</div>-->

				<!--<div id="section-contact" class="page-section notoppadding">

					<div class="row noleftmargin norightmargin bottommargin-lg common-height">
						<div id="headquarters-map" class="col-md-8 col-sm-6 gmap hidden-xs"></div>
						<div class="col-md-4 col-sm-6" style="background-color: #282828;">
							<div class="col-padding max-height">
								<h3 class="font-body t400 ls1">Our Headquarters</h3>

								<div style="font-size: 16px; line-height: 1.7;">
									<address style="line-height: 1.7;">
										<strong>North America:</strong><br>
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address>

									<div class="clear topmargin-sm"></div>

									<address style="line-height: 1.7;">
										<strong>Europe:</strong><br>
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address>

									<div class="clear topmargin"></div>

									<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
									<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
									<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com
								</div>
							</div>
						</div>
					</div>

					<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
					<script type="text/javascript" src="../js/jquery.gmap.js"></script>

					<script type="text/javascript">

						jQuery(window).load( function(){
							jQuery('#headquarters-map').gMap({
								address: 'Melbourne, Australia',
								maptype: 'ROADMAP',
								zoom: 14,
								markers: [
									{
										address: "Melbourne, Australia",
										html: "Melbourne, Australia",
										icon: {
											image: "images/icons/map-icon-red.png",
											iconsize: [32, 32],
											iconanchor: [14,44]
										}
									}
								],
								doubleclickzoom: false,
								controls: {
									panControl: false,
									zoomControl: false,
									mapTypeControl: false,
									scaleControl: false,
									streetViewControl: false,
									overviewMapControl: false
								},
								styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"30"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"gamma":"0.00"},{"lightness":"74"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"3"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
							});
						});

					</script>

					<div class="container clearfix">

						<div class="divcenter topmargin" style="max-width: 850px;">

							<div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

								<div class="form-process"></div>

								<div class="col_half">
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control border-form-control required" placeholder="Name" />
								</div>
								<div class="col_half col_last">
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control border-form-control" placeholder="Email Address" />
								</div>

								<div class="clear"></div>

								<div class="col_one_third">
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control border-form-control" placeholder="Phone" />
								</div>

								<div class="col_two_third col_last">
									<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control border-form-control" placeholder="Subject" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<textarea class="required sm-form-control border-form-control" id="template-contactform-message" name="template-contactform-message" rows="7" cols="30" placeholder="Your Message"></textarea>
								</div>

								<div class="col_full center">
									<button class="button button-border button-circle t500 noleftmargin topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
									<br>
									<small style="display: block; font-size: 13px; margin-top: 15px;">We'll do our best to get back to you within 6-8 working hours.</small>
								</div>

								<div class="clear"></div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
								</div>

							</form>

							<script type="text/javascript">

								$("#template-contactform").validate({
									submitHandler: function(form) {
										$('.form-process').fadeIn();
										$(form).ajaxSubmit({
											target: '#contact-form-result',
											success: function() {
												$('.form-process').fadeOut();
												$(form).find('.sm-form-control').val('');
												$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
												SEMICOLON.widget.notifications($('#contact-form-result'));
											}
										});
									}
								});

							</script>

						</div>

					</div>

				</div>-->

			</div>

			<!-- Modal -->
			<div class="modal1 mfp-hide" id="legalDisclaimer">
				<div class="block divcenter" style="background-color: #FFF; max-width: 500px;">
					<div class="feature-box fbox-center fbox-effect nobottomborder nobottommargin" style="padding: 40px;">
						<!--<div class="fbox-icon">
							<a href="#"><i class="icon-screen i-alt"></i></a>
						</div>-->
						<h3>Legal &amp; Disclaimer</h3>

						<div class="container-fluid">
							<h4>About this site</h4>
							<p>
								S.L.A.D., LLC. website is for the use of our clients and of other individuals and businesses

	interested in using our product and services. We reserve the right to change these terms and

	conditions at any time. Any such changes will take effect when posted on this website and it is

	your responsibility to read the terms and conditions on each occasion you use this website and

	your continued use of this website shall signify your acceptance to be bound by the latest terms

	and conditions.
							</p>
						</div>

						<div class="container-fluid">
							<h4>Disclaimer</h4>
							<p>
								The content of this Website is for general information purposes only. S.L.A.D., LLC. will take

steps to ensure that the information provided is up-to- date and accurate, no warranty, express or

implied, is given.
							</p>
						</div>

						<div class="container-fluid">
							<h4>Intellectual Property</h4>
							<p>
								All copyright and all other intellectual property rights in S.L.A.D. product and services content

contained on this website shall remain at all times vested in S.L.A.D., LLC. or our licensors. You

acknowledge and agree that our product and services content contained on this website is made

available for your commercial use only. You further acknowledge that any other use of the

S.L.A.D. product and services is strictly prohibited and you agree not to (and agree not to assist

or facilitate any third party to) copy, reproduce, transmit, publish, display, distribute,

commercially exploit or create derivative works of such S.L.A.D. product, services and content.
							</p>
						</div>

						<div class="container-fluid">
							<h4>Acceptable use</h4>
							<p>
								It is impossible to provide an exhaustive list of exactly what constitutes acceptable and

unacceptable use of this website. In general, we will not tolerate any use of our website which

damages or is likely to damage S.L.A.D. reputation, the availability or integrity of the website or

which causes us or threatens to cause us to incur any legal, tax or regulatory liability.
							</p>
						</div>

					</div>
					<!--<div class="section center nomargin" style="padding: 30px;">
						<a href="#" class="button" onClick="$.magnificPopup.close();return false;">Don't Show me Again</a>
					</div>-->
				</div>
			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark noborder">

			<div class="container center">
				<div class="footer-widgets-wrap">

					<div class="row divcenter clearfix">

						<div class="col-md-6">

							<div class="widget clearfix">
								<h4>Site Links</h4>

								<ul class="list-unstyled footer-site-links nobottommargin">
									<li><a href="#" data-scrollto="#wrapper" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Top</a></li>
									<li><a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">About</a></li>
									<li><a href="#" data-scrollto="#section-examples" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Why Us?</a></li>
									<li><a href="#" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Known Facts</a></li>
									<li><a href="#legalDisclaimer" data-lightbox="inline">Legal Disclaimer</a></li>
									<li><a href="mailto:info@safetylightalertingdrivers.com">Contact</a></li>
								</ul>
							</div>

						</div>

						<!--<div class="col-md-4">

							<div class="widget clearfix">
								<h4>Subscribe</h4>

								<div id="widget-subscribe-form-result" data-notify-type="success" data-notify-msg=""></div>
								<form id="widget-subscribe-form" action="../include/subscribe.php" role="form" method="post" class="nobottommargin">
									<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control input-lg not-dark required email" placeholder="Your Email Address">
									<button class="button button-border button-circle button-light topmargin-sm" type="submit">Subscribe Now</button>
								</form>
								<script type="text/javascript">
									$("#widget-subscribe-form").validate({
										submitHandler: function(form) {
											$(form).find('.input-group-addon').find('.icon-email2').removeClass('icon-email2').addClass('icon-line-loader icon-spin');
											$(form).ajaxSubmit({
												target: '#widget-subscribe-form-result',
												success: function() {
													$(form).find('.input-group-addon').find('.icon-line-loader').removeClass('icon-line-loader icon-spin').addClass('icon-email2');
													$('#widget-subscribe-form').find('.form-control').val('');
													$('#widget-subscribe-form-result').attr('data-notify-msg', $('#widget-subscribe-form-result').html()).html('');
													SEMICOLON.widget.notifications($('#widget-subscribe-form-result'));
												}
											});
										}
									});
								</script>
							</div>

						</div>-->

						<div class="col-md-6">

							<div class="widget clearfix">
								<h4>Contact</h4>

								<p class="lead"><a href="mailto:info@safetylightalertingdrivers.com">info@safetylightalertingdrivers.com</a></p>

								<!--<div class="center topmargin-sm">
									<a href="#" class="social-icon inline-block noborder si-small si-facebook" title="Facebook">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="#" class="social-icon inline-block noborder si-small si-twitter" title="Twitter">
										<i class="icon-twitter"></i>
										<i class="icon-twitter"></i>
									</a>
									<a href="#" class="social-icon inline-block noborder si-small si-github" title="Github">
										<i class="icon-github"></i>
										<i class="icon-github"></i>
									</a>
									<a href="#" class="social-icon inline-block noborder si-small si-pinterest" title="Pinterest">
										<i class="icon-pinterest"></i>
										<i class="icon-pinterest"></i>
									</a>
								</div>-->
							</div>

						</div>

					</div>

				</div>
			</div>

			<div id="copyrights">
				<div class="container center clearfix">
					Copyright SLAD 2017 | All Rights Reserved
				</div>
			</div>

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Start of SimpleHitCounter Code -->
<div align="center" style="display: none;"><img src="http://simplehitcounter.com/hit.php?uid=2261958&f=16777215&b=0" border="0" height="18" width="83"></div>
<!-- End of SimpleHitCounter Code -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="{{ asset('js/sladJs/functions.js') }}"></script>

</body>
</html>
